from pathlib import Path

fileref = open(Path(__file__).parent.parent / "data" / "day9_input.txt")
lines = fileref.readlines()

mlist = []
n = len(lines)
for i in range(n):
    str1 = lines[i].rstrip()
    l = []
    l.append(str1[0])
    l.append(int(str1[2]))
    mlist.append(l)
# print(mlist)


def dist(hx, hy, tx, ty):
    d = ((hx - tx) * (hx - tx) + (hy - ty) * (hy - ty)) ** 0.5
    return d


def movetail(hx, hy, tx, ty):
    txy = []
    if (hy == ty) and (hx != tx):
        if tx < hx:
            tx = tx + 1
        else:
            tx = tx - 1
    elif (hx == tx) and (hy != ty):
        if ty < hy:
            ty = ty + 1
        else:
            ty = ty - 1
    elif (hx != tx) and (hy != ty):
        if (tx < hx) and (ty < hy):
            tx = tx + 1
            ty = ty + 1
        elif (tx < hx) and (ty > hy):
            tx = tx + 1
            ty = ty - 1
        elif (tx > hx) and (ty > hy):
            tx = tx - 1
            ty = ty - 1
        elif (tx > hx) and (ty < hy):
            tx = tx - 1
            ty = ty + 1
    txy.append(tx)
    txy.append(ty)
    return txy


# Starting point
H = [0, 0]
T = [0, 0]
dcount = {}
dcount["00"] = 1

for i in range(n):

    dir = mlist[i][0]
    val = mlist[i][1]
    print(f"dir = {dir}, val ={val}")
    if dir == "R":
        for j1 in range(val):
            H[0] = H[0] + 1
            H[1] = H[1] + 0
            if dist(H[0], H[1], T[0], T[1]) >= 2:
                txy = movetail(H[0], H[1], T[0], T[1])
                T[0] = txy[0]
                T[1] = txy[1]
                p = str(txy[0]) + str(txy[1])
                if p not in dcount:
                    dcount[p] = 0
                    dcount[p] = dcount[p] + 1
            else:
                continue
    elif dir == "L":
        for j2 in range(val):
            H[0] = H[0] - 1
            H[1] = H[1] + 0
            if dist(H[0], H[1], T[0], T[1]) >= 2:
                txy = movetail(H[0], H[1], T[0], T[1])
                T[0] = txy[0]
                T[1] = txy[1]
                p = str(txy[0]) + str(txy[1])
                if p not in dcount:
                    dcount[p] = 0
                    dcount[p] = dcount[p] + 1
            else:
                continue
    elif dir == "U":
        for j3 in range(val):
            H[0] = H[0] + 0
            H[1] = H[1] + 1
            if dist(H[0], H[1], T[0], T[1]) >= 2:
                txy = movetail(H[0], H[1], T[0], T[1])
                T[0] = txy[0]
                T[1] = txy[1]
                p = str(txy[0]) + str(txy[1])
                if p not in dcount:
                    dcount[p] = 0
                    dcount[p] = dcount[p] + 1
            else:
                continue
    elif dir == "D":
        for j4 in range(val):
            H[0] = H[0] + 0
            H[1] = H[1] - 1
            if dist(H[0], H[1], T[0], T[1]) >= 2:
                txy = movetail(H[0], H[1], T[0], T[1])
                T[0] = txy[0]
                T[1] = txy[1]
                p = str(txy[0]) + str(txy[1])
                if p not in dcount:
                    dcount[p] = 0
                    dcount[p] = dcount[p] + 1
            else:
                continue

    print(f"H: x = {H[0]}, y = {H[1]}, T: x = {T[0]}, y = {T[1]}")

pos = list(dcount.keys())
count = len(pos)
print(count)

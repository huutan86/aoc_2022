"""Source code to solve day 10 challenge.
"""
from dataclasses import dataclass
from pathlib import Path
from typing import List

import networkx as nx

from scripts._util import time_it


@dataclass(frozen=True)
class Node:
    r: int
    c: int


def day12() -> None:
    day12_input_file_name = Path(__file__).parent.parent / "data" / "day12_input.txt"
    G = nx.DiGraph()
    row_idx = 0
    num_cols = 0
    all_a_nodes = []
    with open(day12_input_file_name, "r") as file:
        for line in file.readlines():
            line = line.strip()
            num_cols = len(line)
            for col_idx, v in enumerate(list(line)):
                cur_node = Node(r=row_idx, c=col_idx)
                G.add_node(cur_node, v=v)
                if v == "S":
                    start_node = cur_node

                if v == "E":
                    end_node = cur_node

                if v in ("S", "a"):
                    all_a_nodes.append(cur_node)
            row_idx += 1

    _add_graph_edges(G, num_rows=row_idx, num_cols=num_cols)

    with time_it("part 1") as _:
        _part1(start_node, end_node, G)

    with time_it("part 2") as _:
        _part2(start_node, end_node, G, all_a_nodes)


def _add_graph_edges(G: nx.DiGraph, num_rows: int, num_cols: int) -> None:
    node_attrs = nx.get_node_attributes(G, "v")
    for node in G.nodes:
        r = node.r
        c = node.c
        a = node_attrs[node]
        v = _val_from_attr(a)

        for r_o, c_o in [(-1, 0), (1, 0), (0, 1), (0, -1)]:
            r1 = r + r_o
            c1 = c + c_o
            if 0 <= r1 < num_rows and 0 <= c1 < num_cols:
                target_node = Node(r=r1, c=c1)
                a1 = node_attrs[target_node]
                v1 = _val_from_attr(a1)
                if v1 <= v + 1:
                    G.add_edge(node, target_node, weight=1)


def _val_from_attr(v: str) -> int:
    if v == "S":
        val = ord("a")
    elif v == "E":
        val = ord("z")
    else:
        val = ord(v)
    return val


def _part1(start_node: Node, end_node: Node, G: nx.DiGraph) -> None:
    """
    --- Day 12: Hill Climbing Algorithm ---
    You try contacting the Elves using your handheld device, but the river you're following must be too low to get a decent signal.

    You ask the device for a heightmap of the surrounding area (your puzzle input). The heightmap shows the local area from above broken into a grid; the elevation of each square of the grid is given by a single lowercase letter, where a is the lowest elevation, b is the next-lowest, and so on up to the highest elevation, z.

    Also included on the heightmap are marks for your current position (S) and the location that should get the best signal (E). Your current position (S) has elevation a, and the location that should get the best signal (E) has elevation z.

    You'd like to reach E, but to save energy, you should do it in as few steps as possible. During each step, you can move exactly one square up, down, left, or right. To avoid needing to get out your climbing gear, the elevation of the destination square can be at most one higher than the elevation of your current square; that is, if your current elevation is m, you could step to elevation n, but not to elevation o. (This also means that the elevation of the destination square can be much lower than the elevation of your current square.)

    For example:

    Sabqponm
    abcryxxl
    accszExk
    acctuvwj
    abdefghi
    Here, you start in the top-left corner; your goal is near the middle. You could start by moving down or right, but eventually you'll need to head toward the e at the bottom. From there, you can spiral around to the goal:

    v..v<<<<
    >v.vv<<^
    .>vv>E^^
    ..v>>>^^
    ..>>>>>^
    In the above diagram, the symbols indicate whether the path exits each square moving up (^), down (v), left (<), or right (>). The location that should get the best signal is still E, and . marks unvisited squares.

    This path reaches the goal in 31 steps, the fewest possible.

    What is the fewest steps required to move from your current position to the location that should get the best signal?

    Your puzzle answer was 350.
    """
    shortest_path = nx.shortest_path(G, start_node, end_node)
    p1_path_len = len(shortest_path) - 1
    print(f"Part1 = {p1_path_len}")


def _part2(start_node: Node, end_node: Node, G: nx.DiGraph, all_a_nodes: List[Node]) -> None:
    """
    --- Part Two ---
    As you walk up the hill, you suspect that the Elves will want to turn this into a hiking trail. The beginning isn't very scenic, though; perhaps you can find a better starting point.

    To maximize exercise while hiking, the trail should start as low as possible: elevation a. The goal is still the square marked E. However, the trail should still be direct, taking the fewest steps to reach its goal. So, you'll need to find the shortest path from any square at elevation a to the square marked E.

    Again consider the example from above:

    Sabqponm
    abcryxxl
    accszExk
    acctuvwj
    abdefghi
    Now, there are six choices for starting position (five marked a, plus the square marked S that counts as being at elevation a). If you start at the bottom-left square, you can reach the goal most quickly:

    ...v<<<<
    ...vv<<^
    ...v>E^^
    .>v>>>^^
    >^>>>>>^
    This path reaches the goal in only 29 steps, the fewest possible.

    What is the fewest steps required to move starting from any square with elevation a to the location that should get the best signal?

    Your puzzle answer was 349.
    """
    shortest_path_len = len(nx.shortest_path(G, start_node, end_node))

    for node in all_a_nodes:
        try:
            cur_len = len(nx.shortest_path(G, node, end_node)) - 1
            if cur_len < shortest_path_len:
                shortest_path_len = cur_len
        except Exception as _:
            pass

    print(f"Part2 = {shortest_path_len}")
